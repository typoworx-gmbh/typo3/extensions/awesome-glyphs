<?php
declare(strict_types=1);
namespace TYPOworx\TnmAwesomeGlyphs\Imaging\IconProvider;

use Psr\Container\ContainerInterface;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Core\Bootstrap;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Imaging\IconProviderInterface;
use TYPO3\CMS\Core\Schema\Struct\SelectItem;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class FontAwesome5ProRegularProvider implements IconProviderInterface
{
    public const PROVIDER_NAME = 'FontAwesome 5 Pro';
    public const PROVIDER_PREFIX = 'FontAwesome5ProRegularProvider';

    private ?FrontendInterface $cache = null;
    private ?string $cacheIdentifier;
    protected array $unicodeMap = [];

    protected string $cssPrefix = 'fa-';
    protected string $glyphSvgUri = 'EXT:if_mainmetall/Resources/Public/Fonts/fontawesome-pro/webfonts/fa-regular-400.svg';
    protected array $glyphVariants = [
        'default' => 'fa',
        'light' => 'fal',
        'regular' => 'far',
        'solid' => 'fas',
        'brands' => 'fab',
    ];
    protected array $glyphWeights = [ 300, 400, 900 ];


    public function __construct(?FrontendInterface $assetsCache = null, ?string $cacheIdentifier = null)
    {
        $this->cache = $assetsCache ?? Bootstrap::createCache('assets');
        $this->cacheIdentifier = $cacheIdentifier ?? static::PROVIDER_PREFIX;

        $this->generateUnicodeMap();
    }

    protected static function instance() : self
    {
        return new (static::class)();
    }

    public static function factory(ContainerInterface $container) : self
    {
        $cache = $container->get('cache.assets');

        return new (static::class)($cache, static::PROVIDER_PREFIX);
    }

    protected function generateMarkup(array $options): string
    {
        $name = $this->getName($options);

        //$glyph = htmlspecialchars($name, ENT_QUOTES, 'UTF-8');
        //return '<span class="icon-unify"><i class="fa fa-' . $glyph . '" data-fa-symbol="' .$glyph. '"></i></span>';

        return sprintf('<span class="icon-unify">%s</span>',$this->getIcon($name));
    }

    public function prepareIconMarkup(Icon $icon, array $options = []) : void
    {
        $icon->setMarkup($this->generateMarkup($options));
        $icon->setAlternativeMarkup('inline', $this->generateInlineMarkup($options));
    }

    protected function generateInlineMarkup(array $options): string
    {
        $icons = $this->generateSvgIcons();
        return $icons[$this->getName($options)] ?? '';
    }

    protected function getName(array $options) : string
    {
        $name = (string)($options['name'] ?? '');

        if ($name === '')
        {
            throw new \InvalidArgumentException('The option "name" is required and must not be empty', 1440754978);
        }

        /* Not available in font-awesome, used as a (blank) placeholder icon */
        if ($name === 'empty-empty')
        {
            return $name;
        }

        if (!$this->hasGlyph($name))
        {
            throw new \InvalidArgumentException('The FontAwesome icon name "' . $name . '" is not defined', 1440754979);
        }

        return $name;
    }

    public static function getAll(): array
    {
        $iconSet = [];
        static::apply($iconSet);

        return $iconSet;
    }

    protected function hasGlyph(?string $name,) : bool
    {
        $found = false;

        if (empty($name))
        {
            return false;
        }

        if (isset($this->unicodeMap[ $name ]))
        {
            return true;
        }
    }

    protected function generateUnicodeMap() : void
    {
        $this->generateGlyphsForSvgFont($this->glyphSvgUri);
    }

    protected function generateGlyphsForSvgFont(string $uri) : void
    {
        $unicodeMapCacheIdentifier = sprintf('%s-%s', $this->cacheIdentifier, 'unicodeMap');

        if ($this->cache !== null)
        {
            if ($this->cache->has($unicodeMapCacheIdentifier))
            {
                $this->unicodeMap = (array)$this->cache->get($unicodeMapCacheIdentifier);
                return;
            }
        }

        $svgUri = GeneralUtility::getFileAbsFileName($uri ?? null);
        if ($svgUri === null)
        {
            throw new \InvalidArgumentException(sprintf('Cannot find SVG-Font with File/URI %s', $uri));
        }

        $svgDoc = $this->getSvgContents($svgUri);
        $glyphs = $svgDoc->query('//svg:glyph[@glyph-name and @unicode]');

        if (empty($glyphs))
        {
            return;
        }

        /** @var \DOMElement $glyph */
        foreach ($glyphs as $glyph)
        {
            $name = $glyph->attributes->getNamedItem('glyph-name')->nodeValue ?? null;
            $unicode = $glyph->attributes->getNamedItem('unicode')->nodeValue ?? null;

            if ($name === null || $unicode === null)
            {
                continue;
            }

            // Convert Unicode-Entity to HexString
            $utf8Char = html_entity_decode($unicode, ENT_COMPAT, 'UTF-8');
            $utf16be = mb_convert_encoding($utf8Char, 'UTF-16BE', 'UTF-8');
            $unicode = implode('', unpack('H*', $utf16be));

            $this->unicodeMap[ $name ] = [
                'name' => $name,
                'group' => static::PROVIDER_NAME,
                'unicode' => sprintf('\\%s', $unicode)
            ];
        }

        if ($this->cache !== null)
        {
            $this->cache->set($unicodeMapCacheIdentifier, $this->unicodeMap);
        }
    }

    protected static function apply(array &$target) : void
    {
        $self = static::instance();
        $self->generateUnicodeMap();

        foreach ($self->unicodeMap as $identifier => $symbols)
        {
            if (empty($symbols['name']))
            {
                continue;
            }

            $target[ $identifier ] = [
                'provider' => static::class,
                'name' => $symbols['name']
            ];
        }
    }

    public static function enableAll(array &$icons) : void
    {
        static::apply($icons);
    }

    protected function getSvgContents(string $source): ?\DOMXPath
    {
        if (empty($xmlFile = GeneralUtility::getFileAbsFileName($source)))
        {
            return null;
        }

        $doc = new \DOMDocument();
        $doc->validateOnParse = false;
        $doc->preserveWhiteSpace = false;
        $doc->strictErrorChecking = false;

        try
        {
            $doc->loadXML(file_get_contents($xmlFile));
        }
        catch (\Throwable $e)
        {
            Throw new \InvalidArgumentException(sprintf('Error loading Web-Font from file %s', $xmlFile));
        }

        $xPath = new \DOMXpath($doc);
        $xPath->registerNamespace('svg', 'http://www.w3.org/2000/svg');

        return $xPath;
    }

    protected function escape(array $strings): array
    {
        return array_map(
            static function ($value): string {
                return htmlspecialchars((string)$value, ENT_QUOTES, 'UTF-8');
            },
            $strings
        );
    }

    protected function getIcon(string $name) :? string
    {
        $methodCacheIdentifier = sprintf('%s-%s', $this->cacheIdentifier, 'iconVariants');

        if ($this->cache !== null)
        {
            if ($this->cache->has($methodCacheIdentifier))
            {
                $iconVariants = $this->cache->get($methodCacheIdentifier);
            }
            else
            {
                $iconVariants = $this->generateSvgIcons();
            }
        }
        else
        {
            $iconVariants = $this->generateSvgIcons();
        }

        return $iconVariants[ $name ] ?? null;
    }

    protected function generateSvgIcons(): array
    {
        $glyphs = [];

        $methodCacheIdentifier = sprintf('%s-%s', $this->cacheIdentifier, 'iconVariants');

        if ($this->cache !== null)
        {
            if ($this->cache->has($methodCacheIdentifier))
            {
                return (array)$this->cache->get($methodCacheIdentifier);
            }
        }

        $doc = $this->getSvgContents($this->glyphSvgUri);
        if ($doc === null)
        {
            throw new \RuntimeException('Fontawsome SVG webfont could not be loaded.', 1612868955);
        }

        // https://www.w3.org/TR/SVG11/fonts.html#FontFaceElementUnitsPerEmAttribute
        $defaultUnitsPerEm = 1000;
        $defaultHeight = (int)($doc->query('//svg:font-face')?->item(0)?->attributes?->getNamedItem('units-per-em')?->nodeValue ?? $defaultUnitsPerEm);
        $defaultWidth = (int)($doc->query('//svg:font')?->item(0)?->attributes?->getNamedItem('horiz-adv-x')->nodeValue);
        $viewBoxXOffset = 0;
        $viewBoxYOffset = (int)($doc->query('//svg:font-face')?->item(0)?->attributes?->getNamedItem('descent')->nodeValue);

        $elements = $doc->query('//svg:glyph[@unicode]') ?? null;

        foreach ($elements as $element)
        {
            if (empty($element))
            {
                continue;
            }

            $name = (string)($element->attributes->getNamedItem('glyph-name')?->nodeValue ?? '');
            $path = (string)($element->attributes->getNamedItem('d')?->nodeValue ?? '');
            $width = (int)($element->attributes->getNamedItem('horiz-adv-x')?->nodeValue ?? $defaultWidth);
            $height = $defaultHeight;

            if (empty($name) || empty($path))
            {
                continue;
            }

            $svg = vsprintf(
                '<svg xmlns="http://www.w3.org/2000/svg" viewBox="%s %s %s %s" class="icon-unify"><path class="icon-color" d="%s" /></svg>',
                $this->escape([
                    $viewBoxXOffset,
                    $viewBoxYOffset,
                    $width,
                    $height,
                    $path,
                ])
            );

            $glyphs[ $name ] = $svg;
        }

        if ($this->cache !== null)
        {
            $this->cache->set($methodCacheIdentifier, $glyphs);
        }

        return $glyphs;
    }

    protected static function createSelectItem(?array $itemsProcFuncRef, ?string $label, ?string $value, ?string $icon = null, ?string $group = null, ?string $description = null) :? SelectItem
    {
        if (empty($itemsProcFuncRef))
        {
            return null;
        }

        return new SelectItem(
            type: $itemsProcFuncRef['config']['type'] ?? 'select',
            label: $label,
            value: $value,
            icon: $icon,
            group: $group,
            description: $description
        );
    }

    public static function getTcaIcons(?array $itemsProcFuncRef) : void
    {
        $addItems = [];
        foreach (static::instance()->unicodeMap as $identifier => $symbol)
        {
            if (empty($symbol['name']))
            {
                continue;
            }

            $addItems[ $identifier ] = self::createSelectItem(
                itemsProcFuncRef: $itemsProcFuncRef,
                label: $symbol['name'],
                value: $symbol['name'],
                icon: $symbol['name']
            );
        }

        ksort($addItems);
        $itemsProcFuncRef['items'] = array_merge([], (array)$itemsProcFuncRef['items'], $addItems);
    }

    public static function getTcaIconVariants(?array $itemsProcFuncRef) : void
    {
        foreach (self::instance()->glyphVariants as $label => $value)
        {
            $itemsProcFuncRef['items'][] = self::createSelectItem($itemsProcFuncRef, ucfirst($label), $value);
        }
    }

    public static function getTcaIconWeights(?array $itemsProcFuncRef) : void
    {
        foreach (self::instance()->glyphWeights as $index => $value)
        {
            $value = (string)$value;

            $itemsProcFuncRef['items'][] = self::createSelectItem(
                itemsProcFuncRef: $itemsProcFuncRef,
                label: $value,
                value: $value
            );
        }
    }
}
