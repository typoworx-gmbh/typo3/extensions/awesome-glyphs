<?php
$EM_CONF[$_EXTKEY] = [
    'title' => '(tnm) Awesome-Glyphs',
    'description' => 'Provides Utilitiy to load own glyphs from web-fonts like FontAwesome easily for IconProvider (Configurations\Icons.php)',
    'category' => 'plugin',
    'author' => 'Gabriel Kaufmann',
    'author_email' => 'info@typoworx.com',
    'author_company' => 'TYPOworx GmbH',
    'state' => 'beta',
    'version' => '0.5',
    'constraints' => [
        'depends' => [
            'typo3' => '10.5-12.5'
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
    'autoload' => [
        'psr-4' => [
            'TYPOworx\TnmCkeditorAddons\\' => 'Classes'
        ],
    ],
];
